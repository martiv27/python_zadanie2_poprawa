import math


# funkcja pobierajaca imie


def check_names():
    user_name = input("Give your name ")

    if user_name == "Marcin":
        print("Hello, ", user_name)
    else:
        print("Hello")


# Klasa rownolegloboku obliczajaca pole i porownowujaca obiekty
class Parallelogram:

    def __init__(self, a: float, h: float):
        self.a = a
        self.h = h

        self.area = a * h

    def __eq__(self, other):
        if isinstance(other, Parallelogram):
            return self.a == other.a and self.h == other.h

        elif isinstance(other, float):
            return self.area == other

        return False


# klasa dla bryly

class Cylinder:

    def __init__(self, r: float, h: float):
        self.r = r
        self.h = h
        self.volume = math.pi * r * r * h

    def __eq__(self, other):
        if isinstance(other, Cylinder):
            return self.volume == other.volume

        elif isinstance(other, float):
            return self.volume == other

        return False


# Drzewo 6 klas
class Vehicle:

    def __init__(self, price: float):
        self.price = price

    def __eq__(self, other):
        if isinstance(other, Vehicle):
            return self.price == other.price

        return False


class Boat(Vehicle):

    def __init__(self, size: float, price: float):
        super().__init__(price)
        self.size = size

    def __eq__(self, other):
        if isinstance(other, Boat):
            return self.size == other.size and super().__eq__(other)
        elif isinstance(other, float):
            return self.size == other and super().__eq__(other)

        return False


class MasterCraft(Boat):

    def __init__(self, size: float, price: float):
        super().__init__(size, price)


class Car(Vehicle):

    def __init__(self, type: str, price: float):
        super().__init__(price)
        self.type = type

    def __eq__(self, other):
        if isinstance(other, Car):
            return self.type == other.type and super().__eq__(other)
        elif isinstance(other, str):
            return self.type == other and super().__eq__(other)

        return False


class Porsche(Car):

    def __init__(self, type: str, price: float):
        super().__init__(type, price)


class Toyota(Car):

    def __init__(self, type: str, price: float):
        super().__init__(type, price)