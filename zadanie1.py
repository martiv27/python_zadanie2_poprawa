# klasa ktora przeciaza metode klasy nadrzednej i uzywa jej za pomoca slowa kluczowego super
class Car:

    def __init__(self):
        pass

    def drive(self):
        print("Car is driving")



# dziedziczenie klasy
class Bmw(Car):
    def __init__(self):
        super().__init__()

    def drive(self):
        super().drive()


# dziedziczenie metody i zmiennych
class Dog:

    def __init__(self, name: str):
        self.name = name

    def stroke_dog(self):
        print(f"I am stroking the dog {self.name}")


class Pomeranian(Dog):

    def __init__(self):
        super().__init__("Pomerianian")

# klasa ktora przeciaza metode i uzywa super
class BmwX6(Bmw):

    def __init__(self):
        super().__init__()

    def drive(self):
        print("Bmw X6 ", end='')
        super().drive()


pomeranian_dog = Pomeranian()
pomeranian_dog.stroke_dog()

bmw = Bmw()
bmw.drive()

bmw_X6 = BmwX6()
bmw_X6.drive()